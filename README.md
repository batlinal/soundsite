# README #

### What is this repository for? ###

* Crypto 2.0 Musings Podcast Alexa Skill

### How do I get set up? ###

* Read about [New Alexa Skills Kit (ASK) Feature: Audio Streaming in Alexa Skills](https://developer.amazon.com/public/community/post/Tx1DSINBM8LUNHY/New-Alexa-Skills-Kit-ASK-Feature-Audio-Streaming-in-Alexa-Skills).
* Configure as per [Audio Player Sample Project](https://github.com/alexa/skill-sample-nodejs-audio-player).

### What are the dependencies? ###

* [SoundCloud Crypto 2.0 Musings Podcast](http://feeds.soundcloud.com/users/soundcloud:users:251969939/sounds.rss).
* [AWS S3 alex-batlin-sound-site Bucket](https://console.aws.amazon.com/s3/home?region=us-east-1#&bucket=alex-batlin-sound-site).
* [AWS Lambda arn:aws:lambda:us-east-1:152133126154:function:soundSite Function](https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/soundSite).
* [AWS CloudWatch /aws/lambda/soundSite Log Group](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logStream:group=/aws/lambda/soundSite).
* [AWS DynomoDB SoundSite Table](https://console.aws.amazon.com/dynamodb/home?region=us-east-1#tables:selected=SoundSite).
* [AWS IAM arn:aws:iam::152133126154:user/alex.batlin User](https://console.aws.amazon.com/iam/home?region=us-east-1#users/alex.batlin) for CLI control of Lambda function.
* [AWS IAM arn:aws:iam::152133126154:role/lambda-dynamo-logs Role](https://console.aws.amazon.com/iam/home?region=us-east-1#roles/lambda-dynamo-logs) for Lamdba full access to CloudWatch and DynomoDB.
* [Alexa amzn1.ask.skill.bc373081-e815-4f25-9d9d-e7e9531ab35c Skill](https://developer.amazon.com/edw/home.html#/skill/amzn1.ask.skill.bc373081-e815-4f25-9d9d-e7e9531ab35c/en_US/info).

### How to deploy and test ###

* Inside the src dir run 'npm test'.
* Use the Alexa Simulator and/or the Lamda test UIs.

### Who do I talk to? ###

* [Alex Batlin](mailto:alex@batlin.com)

### How do I use the skill? ###

Companion podcast playback skill to Alex Batlin's Crypto 2.0 Musings blog about blockchain technology, distributed ledgers, virtual currencies and smart contracts. Requires audio player capable devices like Amazon Echo, Echo Dot and Tap.

Once you have enabled this skill by using it's invocation name 'crypto musings' (e.g. Alexa, enable crypto musings.), you can invoke it to start podcast playback by simply saying - Alexa, crypto musings, or you can prepend words like begin, launch, open, play, resume, start, start playing to the invocation name for a more natural sounding instruction e.g. Alexa, start playing crypto musings. Once the skill is invoked, you can ask for help at any time by saying help, help me or can you help me.

When you invoke this skill for the first time, or re-invoke it after listening to all the episodes in the podcast till the end, you will hear a quick introduction message. At any point you can say play to start playing latest podcast episode first, followed by increasing in age order episodes, or you can say stop or cancel to stop skill invocation without starting playback.

Once you start playing an episode of the podcast, you can say stop (or never mind, forget it, off, shut up, pause, cancel) to stop playback. To resume, say either play or resume, continue, keep going. Say next, skip or skip forward to play next queued up episode, or to play the previous one say previous, go back, skip back or back up. To re-queue episodes in latest episode first order and play first episode, say either start over or restart, or start again.

You can loop the current episode by saying loop or loop on, and cancel looping by saying loop off. To shuffle episode playback into random order say shuffle, shuffle on or shuffle mode, and to get back to latest episode first order say shuffle off, stop shuffling or turn off shuffle. 

You can also use Voice Remote for Amazon Echo to stop/pause, start, and skip episodes forwards or backwards.

### How do I test the skill? ###

* Enable the skill by using it's invocation name 'crypto musings'.
	* e.g. Alexa, enable crypto musings.
* Optionally login to [Alexa Web App](https://alexa.amazon.com) to view associated skill cards.
* Start listening to the podcast, also known as invokign the skill, by simply saying to Alexa the skill invocation name 'crypto musings'.
	* e.g. Alexa, crypto musings.
* You can also invoke the skill by pre-pending one of these words: begin | launch | open | play | resume | start | start playing to the invocation name.
	* e.g. Alexa, start playing crypto musings.
* When you invoke the skill for the first time, or re-invoke it after listening to all the episodes in the podcast till the end, you will hear a quick introduction message. At any point you can say one of the following words:
	* play : start playing latest podcast.
	* stop | cancel : play goodbuy message and stop skill invocation without starting playback.
* When you re-invoke the skill after only partially listening to the podcast, you will be asked if you want to resume playback of the podcast you were previosuly listening to. You can say any of the following words:
	* yes | yes please | sure : start playing the podcast episode from where you left off.
	* no | no thanks : start playing first queued up from the top podcast.
	* stop | cancel : play goodbuy message and end skill invocation.
* During podcast episode playback, you can say at any one time any of these words:
	* stop | never mind | forget it | off | shut up | pause | cancel : stop podcast episode playback.
	* next | skip | skip forward : play next queued up episode.
	* previous | go back | skip back | back up : play previous queued up episode.
	* play | resume | continue | keep going : continue playing the podcast episode from where you left off.
	* loop | loop on : loop currently playing episode.
	* loop off : stop looping currently playing episode.
	* shuffle | shuffle on | shuffle mode : randomise episode playback order.
	* shuffle off | stop shuffling | turn off shuffle : play episodes in latest episode first order.
	* start over : start over / restart / start again : re-queue episodes in latest episode first order and play first episode.
* If you have a Voice Remote for Amazon Echo, you can press any of the following buttons:
	* play : continue playing the podcast episode from where you left off.
	* pause | stop : stop podcast episode playback.
	* next : play next queued up episode.
	* previous : play previous queued up episode.
* Once the skill is invoked, you can ask for help at any time by saying help | help me | can you help me.