'use strict';

module.exports = Object.freeze({
    
    // App-ID.
    appId : 'amzn1.ask.skill.bc373081-e815-4f25-9d9d-e7e9531ab35c',
    
    //  DynamoDB Table name
    dynamoDBTableName : 'SoundSite',

    // Alex Batlin photo
    photoImageUrl : 'https://s3.amazonaws.com/alex-batlin-sound-site/alex-batlin.png',

    // Alex Batlin bio audio
    bioAudioUrl : 'https://s3.amazonaws.com/alex-batlin-sound-site/alex-batlin-bio.m4a',
    
    /*
     *  States:
     *  START_MODE : Welcome state when the audio list has not begun.
     *  PLAY_MODE :  When a playlist is being played. Does not imply only active play.
     *               It remains in the state as long as the playlist is not finished.
     *  RESUME_DECISION_MODE : When a user invokes the skill in PLAY_MODE with a LaunchRequest,
     *                         the skill provides an option to resume from last position, or to start over the playlist.
     */
    states : {
        START_MODE : '',
        PLAY_MODE : '_PLAY_MODE',
        RESUME_DECISION_MODE : '_RESUME_DECISION_MODE'
    }
});