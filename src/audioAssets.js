'use strict';

// Audio Source - Crypto 2.0 Musings Podcast : http://feeds.soundcloud.com/users/soundcloud:users:251969939/sounds.rss

var audioData = [
    {
        'title' : 'Episode 39 - Autonomous Validators',
        'url' : 'https://feeds.soundcloud.com/stream/281238005-user-253750088-autonomous-validators.m4a',
        'image' : 'https://i1.sndcdn.com/artworks-000180503524-5l1p7x-original.jpg',
        'blog' : 'https://www.linkedin.com/pulse/crypto-20-musings-autonomous-validators-alex-batlin'
    },
    {
        'title' : 'Episode 38 - Privacy and Confidentiality on Blockchain',
        'url' : 'https://feeds.soundcloud.com/stream/282329293-user-253750088-privacy-and-confidentiality-on-blockchain.m4a',
        'image' : 'https://i1.sndcdn.com/artworks-000181810314-hwq6gm-original.jpg',
        'blog' : 'https://www.linkedin.com/pulse/crypto-20-musings-privacy-confidentiality-alex-batlin'
    },
    {
        'title' : 'Episode 37 - Central Bank Digital Cash',
        'url' : 'https://feeds.soundcloud.com/stream/281235765-user-253750088-central-bank-digital-cash.m4a',
        'image' : 'https://i1.sndcdn.com/artworks-000180502551-npu81q-original.jpg',
        'blog' : 'https://www.linkedin.com/pulse/crypto-20-musings-central-bank-digital-cash-alex-batlin'
    }
];

module.exports = audioData;